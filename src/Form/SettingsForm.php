<?php

namespace Drupal\notify_to_slack\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form to collect the url, channel name and other details.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * NodeType storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a \Drupal\notify_to_slack\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler, RendererInterface $renderer) {
    parent::__construct($config_factory);
    $this->nodeTypeStorage = $entityTypeManager->getStorage('node_type');
    $this->moduleHandler = $moduleHandler;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'notify_slack_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'notify_to_slack.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('notify_to_slack.settings');
    $rendered_token_tree = '';
    $token_tree = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['node', 'user'],
      '#global_types' => TRUE,
    ];
    $rendered_token_tree = $this->renderer->render($token_tree);
    $form['notify_slack_incoming_webhook_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Incoming webhook url'),
      '#default_value' => $config->get('notify_slack_incoming_webhook_url'),
      '#description' => $this->t('Enter incoming webhook url from slack account.'),
    ];
    $form['notify_slack_channel_for_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slack channel'),
      '#default_value' => $config->get('notify_slack_channel_for_message'),
      '#description' => $this->t('Enter slack channel for notification.'),
    ];
    $form['webhook_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the Bot'),
      '#default_value' => $config->get('webhook_name'),
      '#description' => $this->t('Enter display name of the notifier.'),
    ];
    $contentTypesList = [];
    foreach ($this->nodeTypeStorage->loadMultiple() as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }

    $form['node_to_notify_on_slack'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content type to notify on slack'),
      '#default_value' => $config->get('node_to_notify_on_slack')?: [],
      '#description' => $this->t('Content type to notify on slack eg. Article'),
      '#options' => $contentTypesList,
    ];
    foreach (array_keys($contentTypesList) as $content) {
      $form[$content . '_list'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Select the channel you want to notify for the @content', ['@content' => $contentTypesList[$content]]),
        '#default_value' => $config->get('node_' . $content . '_channel'),
        '#description' => $this->t('Keep empty if want to use the default one'),
        '#states' => [
          'visible' => [
            ':input[name="node_to_notify_on_slack[' . $content . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
    $operations = [
      'create' => 'Create',
      'update' => 'Update',
      'delete' => 'Delete',
    ];

    $form['state_to_notify'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the states which need notification'),
      '#default_value' => $config->get('state_to_notify')?: [],
      '#options' => $operations,
    ];

    foreach (array_keys($operations) as $text) {
      $form[$text . '_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Message for @action.', ['@action' => $text]),
        '#default_value' => $config->get('node.' . $text . '_message'),
        '#states' => [
          'visible' => [
            ':input[name="state_to_notify[' . $text . ']"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="state_to_notify[' . $text . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form[$text . '_message']['#description'] = $this->t('Enter token for @action message eg.[node:type].', ['@action' => $text]);
      $form[$text . '_message']['#description'] = $this->t('This field supports tokens. @browse_tokens_link', ['@browse_tokens_link' => $rendered_token_tree]);
    }

    $form['user_entity_to_notify_on_slack'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify to slack about users.'),
      '#default_value' => $config->get('user_entity_to_notify_on_slack')?: '',
      '#description' => $this->t('Select to notify about user create, update or delete.'),
    ];

    $form['user_actions_to_notify'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the states which need notification'),
      '#default_value' => $config->get('user_actions_to_notify')?: [],
      '#options' => $operations,
      '#states' => [
        'visible' => [
          ':input[name="user_entity_to_notify_on_slack"]' => ['checked' => TRUE],
        ],
      ],
    ];

    foreach (array_keys($operations) as $text) {
      $form[$text . '_user_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Message for user @action.', ['@action' => $text]),
        '#default_value' => $config->get('user.' . $text . '_message'),
        '#states' => [
          'visible' => [
            ':input[name="user_actions_to_notify[' . $text . ']"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="user_actions_to_notify[' . $text . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form[$text . '_user_message']['#description'] = $this->t('Enter token for @action message eg.[user:name].', ['@action' => $text]);
      $form[$text . '_user_message']['#description'] = $this->t('This field supports tokens. @browse_tokens_link', ['@browse_tokens_link' => $rendered_token_tree]);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('notify_to_slack.settings')
      ->set('notify_slack_incoming_webhook_url', $form_state->getValue('notify_slack_incoming_webhook_url'))
      ->set('notify_slack_channel_for_message', $form_state->getValue('notify_slack_channel_for_message'))
      ->set('node_to_notify_on_slack', $form_state->getValue('node_to_notify_on_slack'))
      ->set('webhook_name', $form_state->getValue('webhook_name'))
      ->set('state_to_notify', $form_state->getValue('state_to_notify'))
      ->set('node.create_message', $form_state->getValue('create_message'))
      ->set('node.update_message', $form_state->getValue('update_message'))
      ->set('node.delete_message', $form_state->getValue('delete_message'))
      ->set('user_entity_to_notify_on_slack', $form_state->getValue('user_entity_to_notify_on_slack'))
      ->set('user_actions_to_notify', $form_state->getValue('user_actions_to_notify'))
      ->set('user.create_message', $form_state->getValue('create_user_message'))
      ->set('user.update_message', $form_state->getValue('update_user_message'))
      ->set('user.delete_message', $form_state->getValue('delete_user_message'))
      ->save();
    foreach (array_keys($form['node_to_notify_on_slack']['#options']) as $content_type) {
      $this->config('notify_to_slack.settings')->set('node_' . $content_type . '_channel', $form_state->getValue($content_type . '_list'))->save();
    }
    parent::submitForm($form, $form_state);
  }

}
